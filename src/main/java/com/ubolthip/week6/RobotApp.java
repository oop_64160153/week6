package com.ubolthip.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot body = new Robot("Body", 'B', 1, 0);
        Robot peter = new Robot("Peter", 'P', 10, 10);
        body.print();
        body.right();
        body.print();
        peter.print();

        for (int y = Robot.Y_MIN; y <= Robot.Y_MAX; y++) {
            for (int x = Robot.X_MIN; x <= Robot.X_MAX; x++) {
                if (body.getX() == x && body.getY() == y) {
                    System.out.print(body.getSymbol());
                } else if (peter.getX() == x && peter.getY() == y) {
                    System.out.print(peter.getSymbol());
                } else {
                    System.out.print('-');
                }
            }
            System.out.println();
        }
    }
}
