package com.ubolthip.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank worawit = new BookBank("worawit", 100.0);
        worawit.print();
        worawit.deposit(50);
        worawit.print();
        worawit.withdraw(50);
        worawit.print();

        BookBank prayood = new BookBank("prayood", 1.0);
        prayood.deposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweet = new BookBank("praweet", 10);
        praweet.deposit(10000000);
        praweet.withdraw(1000000);
        praweet.print();

    }
}
