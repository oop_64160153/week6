package com.ubolthip.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {

    @Test
    public void shouldDepositSuccess() {
        BookBank book = new BookBank("name", 0);
        boolean result = book.deposit(100);
        assertEquals(true, result);
        assertEquals(100.0, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldDepositNegative() {
        BookBank book = new BookBank("name", 0);
        boolean result = book.deposit(-100);
        assertEquals(false, result);
        assertEquals(0, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldDepositWithdrawSuccess() {
        BookBank book = new BookBank("name", 0);
        book.deposit(100);
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(50, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldDepositWithdrawNegative() {
        BookBank book = new BookBank("name", 0);
        book.deposit(100);
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(100, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldWithdrawOvergetBalance() {
        BookBank book = new BookBank("name", 0);
        book.deposit(50);
        boolean result = book.withdraw(100);
        assertEquals(false, result);
        assertEquals(50, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldWithdraw100getBalance100() {
        BookBank book = new BookBank("name", 0);
        book.deposit(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0, book.getBalance(), 0.00001);
    }

}
